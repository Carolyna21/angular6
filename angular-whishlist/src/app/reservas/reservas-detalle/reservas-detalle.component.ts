import { Component, OnInit } from '@angular/core';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.module';

class DestinosApiClientViejo {
   getById(id: String): DestinoViaje {
  console.log('llamando por la clase vieja!');
  return null;
  }
}
interface AppConfig
  apiEndpoint: string
}
 cost APP_CONFIG_VALUE: AppConfig = {
   aapiEndpoint:'mi:_api.com'
   };
   const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

class DestinosApiClientDecorated extends DestinosApiClient {
  constructor(@Inject(APP_CONFIG) private config: AppConfig,
   store: Store<AppState>){
   super(store); 
}
getById(id:string): DestinoViaje {
   console.log('llamando por la clase decorada!'); 
   console.log('config: ' + this.config.apiEndpoint);
   return super.getById(id);
  }
}
@Component({
  selector: 'app-reservas-detalle',
  templateUrl: './reservas-detalle.component.html',
  styleUrls: ['./reservas-detalle.component.css']
  providers: [ 
    { provide: DestinosApiClient, useClass: DestinosApiClientDecorated },
    { provide: DestinosApiClientViejo, useExisting: DestinosApiClient },
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE }
   ]
})

export class ReservasDetalleComponent implements OnInit {
  destino: DestinoViaje;

constructor(private route: ActivatedRoute, private destinosApiClient: DestinosApiClient) {}
  
ngOnInit(): {
    const id = this.route.snapshot.paramMap.get('id');
      this.destino = this.destinosApiClient.getById(id);
    }
}
