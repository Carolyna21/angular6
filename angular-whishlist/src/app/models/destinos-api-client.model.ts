import { DestinoViaje } from './destino-viaje.model';
import {Injectable, Inject, forwardRef} from '@angular/core';
import { Store } from '@ngrx/store';
import {NuevoDestinoAction,ElegidoFavoritoAction} from './destinos-viajes-state.models';
import {AppState, APP_CONFIG, AppConfig, db} from './../app.module';
import { HttpRequest, HttpHeaders, HttpClient, HttpEvent, HttpResponse } from '@angular/common/http';


@Injectable()

export class DestinosApiClient {
 
 
  constructor(private store: Store<AppState>){
  }
  add(d:DestinoViaje){
    this.store.dispatch(new NuevoDestinoAction(d));
       
      }
  
  elegir(d: DestinoViaje) {
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }
}